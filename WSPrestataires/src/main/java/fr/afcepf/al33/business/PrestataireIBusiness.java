package fr.afcepf.al33.business;

import java.util.List;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import Dto.Periode;
import fr.afcepf.al33.entity.Prestataire;

public interface PrestataireIBusiness {
	public List<Prestataire> getPrestatairesByTypePrestation(Integer idPrestation);	
	public List<Prestataire> getPrestatairesByTypesPrestationsAndDisponibilityPeriode(Periode periode, Integer idPrestation);
	public Prestataire reserverPrestatairePourPeriode(Periode periode, Integer idPrestataire, String societe);
	Prestataire getPrestataire(Integer idPrestataire);
}
