package fr.afcepf.al33.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afcepf.al33.dao.TypePrestationDao;
import fr.afcepf.al33.entity.TypePrestation;

@Service
public class TypePrestationBusiness implements TypePrestationIBusiness {

	@Autowired
	private TypePrestationDao dao;

	@Override
	public List<TypePrestation> getTypesPrestations() {
		return (List<TypePrestation>) dao.findAll();
	}

}
