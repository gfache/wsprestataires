package fr.afcepf.al33.dto;

import java.io.Serializable;

public class PrestataireReference implements Serializable {
	
	private static final long serialVersionUID = 1L; // nécessaire si utilisation avec RMI
	
	private int id;

	public PrestataireReference() {
		super();
	}

	public PrestataireReference(int id) {
		super();
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
		
}
