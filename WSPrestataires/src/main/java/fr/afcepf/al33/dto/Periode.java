package fr.afcepf.al33.dto;

import java.io.Serializable;
import java.util.Date;

public class Periode implements Serializable {
	private static final long serialVersionUID = 1L; // nécessaire si utilisation avec RMI
    private Date dateDebut;
    private Date dateFin;
        
	public Periode() {
		super();
	}

	public Periode(Date start, Date end) {
		super();
		this.dateDebut = start;
		this.dateFin = end;
	}
	
	public Date getDateDebut() {
		return dateDebut;
	}
	public void setDateDebut(Date start) {
		this.dateDebut = start;
	}
	public Date getDateFin() {
		return dateFin;
	}
	public void setDateFin(Date end) {
		this.dateFin = end;
	}

}
